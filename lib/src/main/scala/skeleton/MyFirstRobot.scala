package skeleton

import robocode._

class MyFirstRobot extends Robot {

  override def run(): Unit = {
    ahead(100)
    turnGunRight(360)
    back(100)
    turnGunRight(360)
  }

  override def onScannedRobot(event: ScannedRobotEvent): Unit = {
    fire(1)
  }

  override def onHitByBullet(event: HitByBulletEvent): Unit = {
    back(10)
  }

  override def onHitWall(event: HitWallEvent): Unit = {
    back(20)
  }

}
