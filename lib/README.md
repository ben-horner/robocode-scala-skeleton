This is a skeleton project using scala and gradle that can build a robot which can be imported into Robocode.

`gradle jar` will build the jar in `lib/build/libs/skeleton.MyFirstRobot-0.0.1.jar`

There are several little details to get right.

The `lib/src/main/resources/skeleton/MyFirstRobot.properties` file has lines which will
* make the name and version of the robot show up correctly in the Robocode app
* add a little documentation (author and description)

The `lib/build.gradle` file has lines which will
* put the robot name into the manifest file (within the jar)
* get the jar name right
* import scala and the robocode api into the project

When you make your own robots with your own package and class names and versions, the items listed above will need to change.